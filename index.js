const imageOne = document.querySelector("#image_1");
const imageTwo = document.querySelector("#image_2");
const imageThree = document.querySelector("#image_3");
const imageFour = document.querySelector("#image_4");
const btnStart = document.querySelector(".start");
let timerBlock = document.getElementById('timer');
btnStart.onclick = function () {
    setInterval(timer, 1000);
    btnStart.style.display = "none";
    classManager();
}
function classManager(){
    setTimeout(shower,0,imageOne);
    setTimeout(hider,3000,imageOne);
    setTimeout(shower,3000,imageTwo);
    setTimeout(hider,6000,imageTwo);
    setTimeout(shower,6000,imageThree);
    setTimeout(hider,9000,imageThree);
    setTimeout(shower,9000,imageFour);
    setTimeout(hider,12000,imageFour);
    setTimeout(classManager,12000);
}
function shower(image){
    image.classList.remove("hidden");
}
function hider(image){
    image.classList.add("hidden");
}
function timer() {
    timerBlock.textContent= parseInt(timerBlock.textContent)+1;
}